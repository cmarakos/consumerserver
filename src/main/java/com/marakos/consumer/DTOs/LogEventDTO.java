package com.marakos.consumer.DTOs;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;

public class LogEventDTO implements Serializable {

    @JsonProperty("version")
    private Integer version;

    @JsonProperty("time")
    private ThriftTimestampDTO time;

    @JsonProperty("message")
    private String message;

    @JsonProperty("level")
    private String level;

    @JsonProperty("browser")
    private String browser;

    @JsonProperty("user")
    private String user;

    public LogEventDTO(Integer version, ThriftTimestampDTO time, String message, String level, String browser, String user) {
        this.version = version;
        this.time = time;
        this.message = message;
        this.level = level;
        this.browser = browser;
        this.user = user;
    }

    public LogEventDTO() {
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public ThriftTimestampDTO getTime() {
        return time;
    }

    public void setTime(ThriftTimestampDTO time) {
        this.time = time;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
