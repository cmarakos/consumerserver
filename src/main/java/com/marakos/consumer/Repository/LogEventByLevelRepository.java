package com.marakos.consumer.Repository;

import com.marakos.consumer.model.LogEventByLevel;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface LogEventByLevelRepository extends CassandraRepository<LogEventByLevel, UUID> {


}
