package com.marakos.consumer.utils;

import com.marakos.consumer.DTOs.LogEventDTO;
import com.marakos.consumer.model.LogEventByDate;
import com.datastax.driver.core.utils.UUIDs;
import com.marakos.consumer.model.LogEventByLevel;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Date;

@Service
public class DtoToModel {

    public LogEventByDate LogEventToLogEventByDate(LogEventDTO logEvent){
        LogEventByDate logEventByDate= new LogEventByDate();
        logEventByDate.setId(UUIDs.timeBased());
        logEventByDate.setTime(new Date(
                logEvent.getTime().getYear(),
                logEvent.getTime().getMonth(),
                logEvent.getTime().getDay(),
                logEvent.getTime().getHour(),
                logEvent.getTime().getMinute(),
                logEvent.getTime().getSecond()
                ));
        logEventByDate.setLevel(logEvent.getLevel());
        logEventByDate.setMessage(logEvent.getMessage());
        logEventByDate.setBrowser(logEvent.getBrowser());
        logEventByDate.setUser(logEvent.getUser());
        logEventByDate.setVersion(logEvent.getVersion());

        return logEventByDate;
    }

    public LogEventByLevel LogEventToLogEventByLevel(LogEventDTO logEvent){
        LogEventByLevel logEventByLevel= new LogEventByLevel();
        logEventByLevel.setId(UUIDs.timeBased());
        logEventByLevel.setTime(new Date(
                logEvent.getTime().getYear(),
                logEvent.getTime().getMonth(),
                logEvent.getTime().getDay(),
                logEvent.getTime().getHour(),
                logEvent.getTime().getMinute(),
                logEvent.getTime().getSecond()
        ));
        logEventByLevel.setLevel(logEvent.getLevel());
        logEventByLevel.setMessage(logEvent.getMessage());
        logEventByLevel.setBrowser(logEvent.getBrowser());
        logEventByLevel.setUser(logEvent.getUser());
        logEventByLevel.setVersion(logEvent.getVersion());

        return logEventByLevel;
    }
}
