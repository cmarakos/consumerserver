package com.marakos.consumer.Service;

import com.marakos.consumer.Repository.LogEventByDateRepository;
import com.marakos.consumer.Repository.LogEventByLevelRepository;
import com.marakos.consumer.model.LogEventByDate;
import com.marakos.consumer.model.LogEventByLevel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LogEventServiceImpl implements LogEventService {

    @Autowired
    LogEventByDateRepository logEventByDateRepository;

    @Autowired
    LogEventByLevelRepository logEventByLevelRepository;

    @Override
    public void saveLogEvenByDate(LogEventByDate logEventByDate) {
        logEventByDateRepository.save(logEventByDate);
    }

    @Override
    public void saveLongEventByLevel(LogEventByLevel logEventByLevel) {
        logEventByLevelRepository.save(logEventByLevel);
    }
}
