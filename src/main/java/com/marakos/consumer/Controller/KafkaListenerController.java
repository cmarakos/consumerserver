package com.marakos.consumer.Controller;

import com.marakos.consumer.DTOs.MessageDTO;
import com.marakos.consumer.Service.LogEventService;
import com.marakos.consumer.utils.DtoToModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Controller;

@Controller
public class KafkaListenerController {

    @Autowired
    LogEventService logEventService;

    @Autowired
    DtoToModel dtoToModel;

    Logger logger = LogManager.getLogger(KafkaListenerController.class);

    @KafkaListener(topics = "EventLogs-topic")
    public void listen(MessageDTO messageDTO) {
        logger.debug("Received Messasge with id : " + messageDTO.getId());

        logger.debug("Attempting to persist Messasge with id : " + messageDTO.getId());
        logEventService.saveLogEvenByDate(dtoToModel.LogEventToLogEventByDate(messageDTO.getLogEvent()));
        logEventService.saveLongEventByLevel(dtoToModel.LogEventToLogEventByLevel(messageDTO.getLogEvent()));

    }
}
