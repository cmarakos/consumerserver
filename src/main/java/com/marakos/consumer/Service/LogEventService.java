package com.marakos.consumer.Service;

import com.marakos.consumer.model.LogEventByDate;
import com.marakos.consumer.model.LogEventByLevel;
import org.springframework.stereotype.Service;

@Service
public interface LogEventService {

     void saveLogEvenByDate(LogEventByDate logEventByDate);

     void saveLongEventByLevel(LogEventByLevel logEventByLevel);


}
