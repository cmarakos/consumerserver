package com.marakos.consumer.Repository;

import java.util.UUID;
import com.marakos.consumer.model.LogEventByDate;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LogEventByDateRepository extends CassandraRepository<LogEventByDate, UUID> {


}
