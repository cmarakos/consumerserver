package com.marakos.consumer.DTOs;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class MessageDTO implements Serializable {

    @JsonProperty("id")
    private int id;

    @JsonProperty("logEventDTO")
    private LogEventDTO logEventDTO;

    public MessageDTO() {
    }

    public MessageDTO(int id, LogEventDTO logEventDTO) {
        this.id = id;
        this.logEventDTO = logEventDTO;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LogEventDTO getLogEvent() {
        return logEventDTO;
    }

    public void setLogEvent(LogEventDTO logEventDTO) {
        this.logEventDTO = logEventDTO;
    }
}
